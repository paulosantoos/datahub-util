# DataHub Util

Simple Java Project to get information of idocs from DataHub logs.

To import this project use the Maven template.

You must set to properties to the system works:

- project.name
- file.path

## project.name
This property contains the name of the project and it is used to build the log's file name

## file.path
This property contais the folder where the logs are.

## Example

- project.name=myproject
- file.path=/home/user/logs/
