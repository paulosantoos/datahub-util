package com.paulo.datahub.constants;

public interface Constants {

    String FILE_NAME_DATE = "%s-stdout.%s.log";

    String FILE_NAME_TODAY = "%s-stdout.log";

    String FILE_PATH = "file.path";

    String PROJECT_NAME = "project.name";
}
