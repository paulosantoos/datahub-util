package com.paulo.datahub;

import com.paulo.datahub.constants.Constants;
import com.paulo.datahub.reader.CustomerReader;
import com.paulo.datahub.reader.IdocReader;
import com.paulo.datahub.util.PropertiesUtil;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Select an option: \n");
//        System.out.println("[1] - Customer");
//        System.out.println("[2] - Price");
        System.out.println("[1] - Count Idoc");
        System.out.println("[2] - Find Idoc");
        System.out.println("[3] - Download Idoc");

        String dateInput;
        int idocType = input.nextInt();

        switch (idocType) {
//            case 1:
//                System.out.println("Reading file....");
//                System.out.println(".....");
//                System.out.println("\n");
//                CustomerReader.read(PropertiesUtil.getInstance().getValue(Constants.FILE_PATH));
//                System.out.println("\n");
//                System.out.println("Completed");
//                break;
//
//            case 2:
//                System.out.println("\n");
//                System.out.println("There's no implementation for now");
//                System.out.println("File path: " + PropertiesUtil.getInstance().getValue(Constants.FILE_PATH));
//                break;

            case 1:
                System.out.println("\n");
                System.out.println("Type the date of the idoc, e.g: YYYY-MM-DD or the word 'today'");
                dateInput = input.next();

                System.out.println("Would you like to print the Idocs number ? (true / false)");
                boolean wantPrint = input.nextBoolean();

                if (dateInput.equalsIgnoreCase("today")) {
                    dateInput = null;
                    System.out.println("Looking for IDOC's from today");
                } else {
                    System.out.println("Looking for IDOC's from " + dateInput);
                }

                IdocReader.countIdoc(dateInput, wantPrint);

                break;
            case 2:
                System.out.println("Type the date of the idoc, e.g: YYYY-MM-DD or the word 'today'");
                dateInput = input.next();

                if (dateInput.equalsIgnoreCase("today")) {
                    dateInput = null;
                    System.out.println("Looking for IDOC's from today");
                } else {
                    System.out.println("Looking for IDOC's from " + dateInput);
                }

                System.out.println("\n");
                System.out.println("Type the Idoc code: ");
                IdocReader.findIdocByCode(input.next(), dateInput);
                break;

            case 3:
                System.out.println("Type the date of the idoc, e.g: YYYY-MM-DD or the word 'today'");
                dateInput = input.next();

                if (dateInput.equalsIgnoreCase("today")) {
                    dateInput = null;
                    System.out.println("Looking for IDOC's from today");
                } else {
                    System.out.println("Looking for IDOC's from " + dateInput);
                }

                System.out.println("\n");
                System.out.println("Type the Idoc code: ");
                IdocReader.downloadIdoc(input.next(), dateInput);
                break;
            default:
                break;


        }

    }

}