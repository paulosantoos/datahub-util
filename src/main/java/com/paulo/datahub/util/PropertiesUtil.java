package com.paulo.datahub.util;

import com.paulo.datahub.constants.Constants;

import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {

    private static PropertiesUtil instance = null;
    private Properties properties = new Properties();

    private PropertiesUtil() throws IOException {
        properties.load(getClass().getClassLoader().getResourceAsStream("config.properties"));
    }

    public static PropertiesUtil getInstance() {
        if (instance == null) {
            try {
                instance = new PropertiesUtil();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        return instance;
    }

    public String getValue(String key) {
        return properties.getProperty(key);
    }

    public String getFile(String key, String date) {
        String filePath = properties.getProperty(key);
        String fileName = properties.getProperty("project.name");

        if (date != null) {
            return String.format(Constants.FILE_NAME_DATE, filePath + fileName, date);
        }
        return String.format(Constants.FILE_NAME_TODAY, filePath + fileName);
    }
}
