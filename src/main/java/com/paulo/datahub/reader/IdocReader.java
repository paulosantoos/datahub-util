package com.paulo.datahub.reader;

import com.paulo.datahub.constants.Constants;
import com.paulo.datahub.util.PropertiesUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IdocReader {

    public static void countIdoc(final String dateFrom, final boolean wantPrint) {
        int idocs = 0;
        String line = "";
        try {
            List<FileInputStream> files = getLogFile(dateFrom);

            for (FileInputStream input : files) {
                CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
                decoder.onMalformedInput(CodingErrorAction.IGNORE);
                InputStreamReader reader = new InputStreamReader(input, decoder);
                BufferedReader bufferedReader = new BufferedReader(reader);

                while ((line = bufferedReader.readLine()) != null) {
                    if (line.startsWith("<?xml")) {
//                    System.out.println(line);
                        Document document = convertStringToXMLDocument(line.trim().replaceFirst("^([\\W]+)<", "<"));
                        idocs += countIdoc(document, wantPrint);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Number of Idocs: " + idocs);
    }

    public static void findIdocByCode(String idocCode, String dateFrom) {
        Boolean thereIs = Boolean.FALSE;
        if (idocCode != null) {
            List<FileInputStream> files = getLogFile(dateFrom);
            thereIs = searchInTheFile(idocCode, files) != null;
        }

        System.out.println("\n");
        System.out.println("IDOC " + idocCode + " found: " + thereIs);
    }

    public static void downloadIdoc(String idocCode, String dateFrom) {
        try {
            List<FileInputStream> files = getLogFile(dateFrom);
            String idocString = searchInTheFile(idocCode, files);
            Document document = convertStringToXMLDocument(idocString.trim().replaceFirst("^([\\W]+)<", "<"));

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(PropertiesUtil.getInstance().getValue(Constants.FILE_PATH) + idocCode + ".xml"));
            transformer.transform(source, result);

            System.out.println("Idoc downloaded in: " + result.getSystemId());
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static int countIdoc(Document xmlDocument, final boolean wantPrint) {
        if (xmlDocument != null) {
            xmlDocument.getDocumentElement().normalize();

            NodeList nList = xmlDocument.getElementsByTagName("DOCNUM");
            if (wantPrint) {
//                System.out.println("----------------------------");
//                System.out.println("Total of IDOC's: " + nList.getLength());
//                System.out.println("----------------------------");
                System.out.println("----------------------------");
                for (int x = 0; nList.getLength() > x; x++) {
                    System.out.println("IDOC Number: " + nList.item(x).getFirstChild().getNodeValue());
                }
                System.out.println("----------------------------");
            }
            return nList.getLength();
        } else {
            System.out.println("Document not found");
        }

        return 0;
    }

    private static Document convertStringToXMLDocument(String xmlString) {
        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder = null;
        try {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            return builder.parse(new InputSource(new StringReader(xmlString)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<FileInputStream> getLogFile(final String dateFrom) {
        String fileName = null;
        if (dateFrom != null) {
            fileName = PropertiesUtil.getInstance().getFile(Constants.FILE_PATH, dateFrom).replace(".log", "");
        } else {
            fileName = PropertiesUtil.getInstance().getFile(Constants.FILE_PATH, dateFrom);
        }
        List<FileInputStream> files = new ArrayList<>();
        List<Path> pathFiles;

        try (Stream<Path> paths = Files.walk(Paths.get(PropertiesUtil.getInstance().getValue(Constants.FILE_PATH)))) {
            String finalFileName = fileName;
            pathFiles = paths.filter(f -> f.toString().startsWith(finalFileName)).collect(Collectors.toList());
            files = convertPathToFileInputStream(pathFiles);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return files;
    }

    private static List<FileInputStream> convertPathToFileInputStream(List<Path> pathFiles) throws FileNotFoundException {
        List<FileInputStream> files = new ArrayList<>();
        for (Path path : pathFiles) {
            files.add(new FileInputStream(new File(path.toUri())));
        }
        return files;
    }

//    private static Boolean searchInTheFile(String word, List<FileInputStream> files) {
//        Boolean thereIs = Boolean.FALSE;
//        try {
//            for (FileInputStream input : files) {
//                CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
//                decoder.onMalformedInput(CodingErrorAction.IGNORE);
//                InputStreamReader reader = new InputStreamReader(input, decoder);
//                BufferedReader bufferedReader = new BufferedReader(reader);
//                String line = "";
//
//                while ((line = bufferedReader.readLine()) != null) {
//                    if (line.contains(word)) {
//                        thereIs = Boolean.TRUE;
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return thereIs;
//    }

    private static String searchInTheFile(String word, List<FileInputStream> files) {

        String idoc = null;
        try {
            for (FileInputStream input : files) {
                CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
                decoder.onMalformedInput(CodingErrorAction.IGNORE);
                InputStreamReader reader = new InputStreamReader(input, decoder);
                BufferedReader bufferedReader = new BufferedReader(reader);
                String line = "";

                while ((line = bufferedReader.readLine()) != null) {
                    if (line.contains(word)) {
                        idoc = line;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return idoc;
    }
    //    private static void printNote(NodeList nodeList) {
//
//        for (int count = 0; count < nodeList.getLength(); count++) {
//
//            Node tempNode = nodeList.item(count);
//
//            // make sure it's element node.
//            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                // get node name and value
//                System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
//                System.out.println("Node Value =" + tempNode.getTextContent());
//
//                if (tempNode.hasAttributes()) {
//
//                    // get attributes names and values
//                    NamedNodeMap nodeMap = tempNode.getAttributes();
//
//                    for (int i = 0; i < nodeMap.getLength(); i++) {
//
//                        Node node = nodeMap.item(i);
//                        System.out.println("attr name : " + node.getNodeName());
//                        System.out.println("attr value : " + node.getNodeValue());
//
//                    }
//
//                }
//
//                if (tempNode.hasChildNodes()) {
//
//                    // loop again if has child nodes
//                    printNote(tempNode.getChildNodes());
//
//                }
//
//                System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");
//
//            }
//
//        }
//
//    }
//
}
