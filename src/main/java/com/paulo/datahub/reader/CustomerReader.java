package com.paulo.datahub.reader;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CustomerReader {

    /*
    Javadoc: http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
     */
    public static void read(final String pathName) {
        try {

            File fXmlFile = new File(pathName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("E1KNA1M");

            System.out.println("----------------------------");
            System.out.println("Total of Customers: " + nList.getLength());
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                System.out.println("\nCurrent Customer:" + (1 + temp));

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    System.out.println("Customer Nr: " + (eElement.getElementsByTagName("KUNNR").getLength() > 0 ? eElement.getElementsByTagName("KUNNR").item(0).getTextContent() : null));
                    System.out.println("Title : " + (eElement.getElementsByTagName("ANRED").getLength() > 0 ? eElement.getElementsByTagName("ANRED").item(0).getTextContent() : null));
                    System.out.println("Name : " + (eElement.getElementsByTagName("NAME1").getLength() > 0 ? eElement.getElementsByTagName("NAME1").item(0).getTextContent() : null));
                    System.out.println("NAME2 : " + (eElement.getElementsByTagName("NAME2").getLength() > 0 ? eElement.getElementsByTagName("NAME2").item(0).getTextContent() : null));
                    System.out.println("Customer Group : " + (eElement.getElementsByTagName("KUKLA").getLength() > 0 ? eElement.getElementsByTagName("KUKLA").item(0).getTextContent() : null));
                    System.out.println("LAND1 : " + (eElement.getElementsByTagName("LAND1").getLength() > 0 ? eElement.getElementsByTagName("LAND1").item(0).getTextContent() : null));
                    System.out.println("NAME3 : " + (eElement.getElementsByTagName("NAME3").getLength() > 0 ? eElement.getElementsByTagName("NAME3").item(0).getTextContent() : null));
                    System.out.println("ORT01 : " + (eElement.getElementsByTagName("ORT01").getLength() > 0 ? eElement.getElementsByTagName("ORT01").item(0).getTextContent() : null));
                    System.out.println("PSTLZ : " + (eElement.getElementsByTagName("PSTLZ").getLength() > 0 ? eElement.getElementsByTagName("PSTLZ").item(0).getTextContent() : null));
                    System.out.println("REGIO : " + (eElement.getElementsByTagName("REGIO").getLength() > 0 ? eElement.getElementsByTagName("REGIO").item(0).getTextContent() : null));
                    System.out.println("SORTL : " + (eElement.getElementsByTagName("SORTL").getLength() > 0 ? eElement.getElementsByTagName("SORTL").item(0).getTextContent() : null));
                    System.out.println("SPRAS : " + (eElement.getElementsByTagName("SPRAS").getLength() > 0 ? eElement.getElementsByTagName("SPRAS").item(0).getTextContent() : null));
                    System.out.println("STRAS : " + (eElement.getElementsByTagName("STRAS").getLength() > 0 ? eElement.getElementsByTagName("STRAS").item(0).getTextContent() : null));
                    System.out.println("TELF1 : " + (eElement.getElementsByTagName("TELF1").getLength() > 0 ? eElement.getElementsByTagName("TELF1").item(0).getTextContent() : null));
                    System.out.println("KATR7 : " + (eElement.getElementsByTagName("KATR7").getLength() > 0 ? eElement.getElementsByTagName("KATR7").item(0).getTextContent() : null));
                    System.out.println("SPRAS_ISO : " + (eElement.getElementsByTagName("SPRAS_ISO").getLength() > 0 ? eElement.getElementsByTagName("SPRAS_ISO").item(0).getTextContent() : null));

                    System.out.println("----------------------------\n");
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error: Cannot find the file: " + pathName);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }
}
